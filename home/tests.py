from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import home_view
import datetime


# Create your tests here.
class LandingTest(TestCase):
    def test_apakah_url_landing_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_landing_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home_view)

    def test_redirect(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_html_sesuai(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home/home.html')
    